section .data
too_long:
    db "Line is too long, it should be no more than 255 symbols", 10, 0

not_found:
    db "Word doesn't found", 10, 0