%define last_word 0
%macro colon 2
	%ifstr %1
	     %ifid %2
	     	    %2:
                	dq last_word
                	%define last_word %2
                	db %1, 0

		%else
			%error "Incorrect name of label!"
		%endif
	%else
	    %error "Incorrect name of word!"
	%endif
%endmacro

