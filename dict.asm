section .text
global find_word
extern string_equals

find_word:
    .check:
        cmp rsi, 0
        je .error
    .find:
        push rsi
        add rsi, 8
        call string_equals
        pop rsi
        cmp rax, 0x1
        je .success
        mov rsi, [rsi]
        jmp .check
    .success:
        mov rax, rsi
        jmp .end
    .error:
        mov rax, 0
    .end:
        ret



