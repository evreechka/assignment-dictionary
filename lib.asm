section .data
    %include "error_messages.inc"
    %DEFINE BUFFER_SIZE 254
    %DEFINE stdout 1
    %DEFINE stderr 2
section .text
global print_dict_word
global print_string
global print_error
global print_char
global print_new_line
global print_uint
global print_int
global read_line
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global string_equals
global string_length
global exit

exit:
    mov rax, 60
    syscall

read_line:
    mov rsi, 0
    .read:
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        cmp rax, 10
        je .end_read
        cmp rsi, 255
        ja .error
        cmp rax, 0
        je .end_read
        mov [rdi+rsi], byte al
        inc rsi
        jmp .read
    .end_read:
        cmp rsi, 255
        ja .error
        mov rax, 1
        mov [rdi + rsi], byte 0
        jmp .end
    .error:
        mov rax, too_long
    .end:
        ret

print_dict_word:
    .print_word:
        push rdi
        call print_string
        call print_space
        call print_hyphen
        call print_space
        pop rdi
    .print_def:
        call string_length
        add rdi, rax
        inc rdi
        call print_string
        call print_new_line
        ret

print_error:
    .start:
        push rsi
        push rax
        push rdx
        push rdi
        call string_length
    .print:
        mov rsi, rdi
        mov rdx, rax
        mov rax, 1
        mov rdi, stderr
        syscall
    .end:
        pop rdi
        pop rdx
        pop rax
        pop rsi
        ret

string_length:
    .start:
        xor rax, rax
    .count:
        cmp byte [rax+rdi], 0
        je .end
        inc rax
        jmp .count
    .end:
        ret

print_string:
    .start:
        call string_length
    .print:
        mov     rsi, rdi
        mov     rdx, rax
        mov     rax, 1
        mov     rdi, stdout
        syscall
    .end:
        ret

print_hyphen:
    mov rdi, '-'
    jmp print_char

print_space:
    mov rdi, ' '
    jmp print_char

print_char:
    .start:
        push rsi
        push rax
        push rdx
        push rdi
    .print:
        mov rsi, rsp
        mov     rdx, 1
        mov     rax, 1
        mov     rdi, stdout
        syscall
    .end:
        pop rdi
        pop rdx
        pop rax
        pop rsi
        ret

print_new_line:
    .start:
        push rdi
    .print:
        mov rdi, 10
        call print_char
    .end:
        pop rdi
        ret

print_uint:
    .start:
        push rax
        push rdx
        push rsi
        mov rax, rdi
        mov rdi, 10
        mov rsi, rsp ; Save the address of rsp in rsi
        dec rsp
        mov [rsp], byte 0
    .int_to_string:
        xor rdx, rdx
        div rdi
        add rdx, '0'
        dec rsp
        mov byte [rsp], dl
        cmp rax, 0
        je .print_num
        jmp .int_to_string
    .print_num:
        mov rdi, rsp
        push rsi
        call print_string
        pop rsi
    .end:
        mov rsp, rsi
        pop rsi
        pop rdx
        pop rax
        ret

print_int:
    .start:
        cmp rdi, 0
        jns print_uint
    .print_minus:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
    .print_num:
        call print_uint
        ret

string_equals:
    .start:
        push rdx
        xor rdx, rdx
    .check_equals:
        cmp byte [rdi+rdx], 0
    	je .equal
        xor rax, rax
        mov al, byte [rdx+rsi]
    	cmp byte [rdi+rdx], al
    	jne .not_equal
    	inc rdx
    	jmp .check_equals
    .equal:
    	cmp byte [rsi+rdx], 0
    	jne .not_equal
    	mov rax, 0x1
    	jmp .end
    .not_equal:
    	mov rax, 0x0
    .end:
        pop rdx
    	ret

read_char:
    .start:
        push 0x0
    .read:
        mov rsi, rsp
        mov rdi, 0
        mov rdx, 1
        mov rax, 0
        syscall
    .end:
        pop rax
        ret

read_word:
    xor rax, rax
    push r8
    xor r8, r8
    .ignore_spaces:
        call read_char
        cmp rax, 0x20
        je .ignore_spaces
        cmp rax, 0x9
        je .ignore_spaces
        cmp rax, 0xA
        je .ignore_spaces
        test rax, rax
        jz .success
    .read:
        cmp r8, rsi
        ja .error
        cmp rax, 0x20
        je .success
        cmp rax, 0x9
        je .success
        cmp rax, 0xA
        je .success
        cmp rax, 0
        je .success
        mov [rdi+r8], rax
        inc r8
        call read_char
        jmp .read
    .success:
        mov [rdi+r8+1], byte 0
        mov rax, rdi
        mov rdx, r8
        jmp .end
    .error:
        mov rax, 0
    .end:
        pop r8
        ret

parse_uint:
    .start:
        push rsi
        xor rax, rax
        xor rdx, rdx
    .parse:
        xor rsi, rsi
        mov sil, byte[rdi+rdx]
        cmp sil, '0'
        jb .end
        cmp sil, '9'
        ja .end
        sub sil, '0'
        imul rax, 10
        add rax, rsi
        inc rdx
        jmp .parse
    .end:
        pop rsi
        ret

parse_int:
    .check_sign:
        cmp byte[rdi], '-'
        jne parse_uint
    .invert:
        inc rdi
        call parse_uint
        cmp rdx, 0
        je .end
        neg rax
        inc rdx
    .end:
        ret

string_copy:
    .start:
        push r8
        push rcx
        call string_length
        inc rax
        cmp rdx, rax
        jl .error
        xor r8, r8
    .copy:
        xor rcx, rcx
        mov cl, byte[rdi+r8]
        mov byte[rsi+r8], cl
        inc r8
        cmp r8, rax
        jl .copy
        dec rax
        jmp .end
    .error:
        xor rax, rax
    .end:
        pop rcx
        pop r8
        ret