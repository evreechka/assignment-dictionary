section .data
    %include "word.inc"
    %include "error_messages.inc"
    buffer: times 256 db 0

section .text
extern read_char
extern read_word
extern find_word
extern print_string
extern exit
extern print_dict_word
extern read_line
extern print_error
global _start

_start:
    .read:
        mov rdi, buffer
        call read_line
        cmp rax, 1
        jne .error
        mov rsi, last_word
        call find_word
        cmp rax, 0
        jne .success
        mov rax, not_found
    .error:
        mov rdi, rax
        call print_error
        jmp .end
    .success:
        add rax, 8
        mov rdi, rax
        call print_dict_word
    .end:
        call exit
