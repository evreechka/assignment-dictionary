all: main.o lib.o dict.o
	ld -o lab2 main.o lib.o dict.o
lib.o: lib.asm
	nasm -felf64 -o lib.o lib.asm
dict.o: dict.asm
	nasm -felf64 -o  dict.o dict.asm
main.o: main.asm colon.inc error_messages.inc word.inc
	nasm -felf64 -o  main.o main.asm
clean:
	rm -f lab2 lib.o dict.o main.o

.PHONY: clean
